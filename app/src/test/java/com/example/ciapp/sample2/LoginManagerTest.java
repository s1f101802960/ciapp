package com.example.ciapp.sample2;

import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.*;

public class LoginManagerTest {
    private LoginManager loginManager;

    @Before
    public void setUp() throws ValidateFailedException{
        loginManager = new LoginManager();
        loginManager.register("Testuser1", "password");
    }

    @Test
    public void testLoginSuccess() throws LoginFailedException,UserNotFoundException, InvalidPasswordException {
        User user = loginManager.login("Testuser1", "password");
        assertThat(user.getUsername(), is("Testuser1"));
        assertThat(user.getPassword(), is("password"));
    }

    @Test(expected = InvalidPasswordException.class)
    public void testLoginWrongPassword() throws LoginFailedException,UserNotFoundException, InvalidPasswordException {
        User user = loginManager.login("testuser1", "1234");
    }

    @Test(expected = LoginFailedException.class)
    public void testLoginUnregisteredUser() throws LoginFailedException,UserNotFoundException, InvalidPasswordException {
        User user = loginManager.login("iniad", "password");
    }
}
